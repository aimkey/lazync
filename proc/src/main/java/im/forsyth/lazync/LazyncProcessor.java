package im.forsyth.lazync;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

public class LazyncProcessor extends AbstractProcessor {

    private static class TypeSpecPackageHolder {

        public TypeSpecPackageHolder(TypeSpec.Builder typeSpecBuilder, String packageName) {
            this.typeSpecBuilder = typeSpecBuilder;
            this.packageName = packageName;
        }

        TypeSpec.Builder typeSpecBuilder;
        String packageName;
    }

    private static Map<String, ClassName> sWrapperClassNames
            = new HashMap<String, ClassName>() {
        {
            put(Lazync.Wrapper.FUTURE_TASK,
                    ClassName.get("java.util.concurrent", "FutureTask"));
            put(Lazync.Wrapper.RX_SINGLE,
                    ClassName.get("io.reactivex", "Single"));
            put(Lazync.Wrapper.GUAVA_LISTENABLE_FUTURE_TASK,
                    ClassName.get("com.google.common.util.concurrent", "ListenableFutureTask"));
            put(Lazync.Wrapper.ANDROID_ASYNC_TASK,
                    ClassName.get("android.os", "AsyncTask"));
        }
    };

    private static Map<String, ClassName> sHellFireClassNames
            = new HashMap<String, ClassName>() {
        {
            put(Lazync.Wrapper.FUTURE_TASK,
                    ClassName.get("im.forsyth.lazync", "Inferno"));
            put(Lazync.Wrapper.RX_SINGLE,
                    ClassName.get("im.forsyth.lazync", "RxInferno"));
            put(Lazync.Wrapper.GUAVA_LISTENABLE_FUTURE_TASK,
                    ClassName.get("im.forsyth.lazync", "TropicalInferno"));
            put(Lazync.Wrapper.ANDROID_ASYNC_TASK,
                    ClassName.get("im.forsyth.lazync", "RobotInferno"));
        }
    };

    private Map<CharSequence, TypeSpecPackageHolder> mGeneratedTypes =
            new HashMap<CharSequence, TypeSpecPackageHolder>();

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(Lazync.class);
        for (Element element : elements) {
            if (element.getKind() != ElementKind.METHOD) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        "@Lazync can only annotate methods", element);
                continue;
            }

            ExecutableElement methodElement = (ExecutableElement) element;
            TypeElement typeElement = (TypeElement) methodElement.getEnclosingElement();
            PackageElement packageElement
                    = processingEnv.getElementUtils().getPackageOf(typeElement);

            if (typeElement.getQualifiedName().length() == 0) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        "Cannot annotate methods with @Lazync in an anonymous class",
                        element);
                continue;
            }

            Lazync ann = methodElement.getAnnotation(Lazync.class);
            if (!Lazync.Wrapper.isSupported(ann.wrapper())) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        "Wrapper is not supported", element);
                continue;
            }

            ParameterizedTypeName returnType;
            if (ann.wrapper().equals(Lazync.Wrapper.ANDROID_ASYNC_TASK)) { // multiple params
                ParameterizedTypeName callbackTypeName =
                        ParameterizedTypeName.get(ClassName.get("im.forsyth.lazync.LazyncTask",
                                "Callback"), TypeName.VOID.box());
                returnType = ParameterizedTypeName.get(sWrapperClassNames.get(ann.wrapper()),
                        callbackTypeName, TypeName.VOID.box(), TypeName.get(methodElement.getReturnType()).box());
            } else { //single param
                returnType = ParameterizedTypeName.get(sWrapperClassNames.get(ann.wrapper()),
                        TypeName.get(methodElement.getReturnType()).box());
            }


            Set<Modifier> modifierSet = new HashSet<Modifier>(methodElement.getModifiers());
            modifierSet.remove(Modifier.ABSTRACT);

            // Start building the method
            MethodSpec.Builder methodSpecBuilder
                    = MethodSpec.methodBuilder(methodElement.getSimpleName().toString())
                    .addModifiers(modifierSet.toArray(new Modifier[]{}))
                    .returns(returnType);

            // Check to see if we've already started generating the containing type
            TypeSpecPackageHolder holder;
            final String generatedClassName;
            switch (ann.generatedClassType()) {
                case STANDALONE_APPEND_TAG:
                    generatedClassName = typeElement.getSimpleName() + ann.tag();
                    holder = mGeneratedTypes.get(generatedClassName);
                    break;
                case STANDALONE_PREPEND_TAG:
                    generatedClassName = ann.tag() + typeElement.getSimpleName();
                    holder = mGeneratedTypes.get(generatedClassName);
                    break;
                case STANDALONE_TAG_ONLY:
                    generatedClassName = ann.tag();
                    holder = mGeneratedTypes.get(generatedClassName);
                    break;
                default:
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                            "Illegal generatedClassType", element);
                    continue;
            }

            TypeSpec.Builder typeSpecBuilder = holder == null ? null : holder.typeSpecBuilder;

            boolean targetIsStatic = methodElement.getModifiers().contains(Modifier.STATIC);
            if (!targetIsStatic) {
                // For the static variant, we add the static modifier and
                // add an instance of the enclosing type as the first parameter
                methodSpecBuilder.addModifiers(Modifier.STATIC);
                methodSpecBuilder.addParameter(
                        ParameterSpec.builder(TypeName.get(typeElement.asType()),
                                "instance").build());
            }

            if (typeSpecBuilder == null) {
                // We have not already started generating this type, so let's create it
                typeSpecBuilder = TypeSpec
                        .classBuilder(generatedClassName)
                        .addModifiers(typeElement.getModifiers().toArray(new Modifier[]{}));
                for (TypeParameterElement typeParameterElement : typeElement.getTypeParameters()) {
                    TypeVariableName tvn = getTypeVariableName(typeParameterElement);
                    typeSpecBuilder.addTypeVariable(tvn);
                }
                mGeneratedTypes.put(generatedClassName,
                        new TypeSpecPackageHolder(typeSpecBuilder,
                                packageElement.getQualifiedName().toString()));
            }

            // If the class has generic param(s), we need to add the param(s) to the method
            if (!targetIsStatic) {
                for (TypeParameterElement typeParameterElement : typeElement.getTypeParameters()) {
                    TypeVariableName tvn = getTypeVariableName(typeParameterElement);
                    methodSpecBuilder.addTypeVariable(tvn);
                }
            }

            // Add generic params to the method
            for (TypeParameterElement typeParamElement : methodElement.getTypeParameters()) {
                methodSpecBuilder.addTypeVariable(getTypeVariableName(typeParamElement));
            }

            StringBuilder argsArrayBuilder = new StringBuilder();
            StringBuilder paramsArrayBuild = new StringBuilder();
            // Add regular params
            for (VariableElement param : methodElement.getParameters()) {
                ParameterSpec.Builder paramSpecBuilder
                        = ParameterSpec.builder(TypeName.get(param.asType()),
                        param.getSimpleName().toString(),
                        param.getModifiers().toArray(new Modifier[]{}));
                methodSpecBuilder.addParameter(paramSpecBuilder.build());
                if (argsArrayBuilder.length() != 0) {
                    argsArrayBuilder.append(", ");
                    paramsArrayBuild.append(", ");
                }
                argsArrayBuilder.append(param.getSimpleName());
                String paramClassString = getParamClassString(param, element);
                if (paramClassString == null) {
                    throw new IllegalStateException("Illegal parameter");
                }
                paramsArrayBuild.append(getParamClassString(param, element));
            }

            // Generate the method code
            ClassName hellFireClassName = sHellFireClassNames.get(ann.wrapper());
            String classOrInstance = !targetIsStatic
                    ? "instance" : typeElement.getSimpleName().toString() + ".class";

            methodSpecBuilder.addStatement(
                "return $T.getInstance().destruction($L, \"$L\", new Class<?>[] {$L}, new Object[] {$L})",
                hellFireClassName, classOrInstance, methodElement.getSimpleName().toString(),
                    paramsArrayBuild.toString(), argsArrayBuilder.toString());

            typeSpecBuilder.addMethod(methodSpecBuilder.build());
        }

        if (roundEnv.processingOver() && !roundEnv.errorRaised()) {
            for (TypeSpecPackageHolder typeSpecPackageHolder : mGeneratedTypes.values()) {
                JavaFile javaFile = JavaFile.builder(typeSpecPackageHolder.packageName,
                        typeSpecPackageHolder.typeSpecBuilder.build()).build();
                try {
                    javaFile.writeTo(processingEnv.getFiler());
                } catch (IOException exception) {
                    throw new RuntimeException("Error processing @Lazync annotations");
                }
            }
        }
        return true;
    }

    // Future use
    private DeclaredType lookupDeclaredType(CharSequence name) {
        return processingEnv.getTypeUtils().getDeclaredType(
                processingEnv.getElementUtils().getTypeElement(name));
    }

    private TypeVariableName getTypeVariableName(TypeParameterElement typeParameterElement) {
        TypeMirror[] boundsArray = typeParameterElement.getBounds().toArray(new TypeMirror[] {});
        TypeName[] boundsArrayName = new TypeName[boundsArray.length];
        for (int i = 0; i < boundsArray.length; i++) {
            boundsArrayName[i] = TypeName.get(boundsArray[i]);
        }
        return TypeVariableName.get(TypeName.get(typeParameterElement.asType()).toString(),
                boundsArrayName);
    }

    private String getParamClassString(VariableElement variableElement,
                                       Element targetElement) {
        switch (variableElement.asType().getKind()) {
            case DECLARED:
            case ARRAY:
                TypeMirror typeMirror = variableElement.asType();
                String[] removeParams = typeMirror.toString().split("<");
                String fullyQualifiedType = removeParams[0];
                String[] parts = fullyQualifiedType.split("\\.");
                String className = parts[parts.length -1];
                return className + ".class";
            case TYPEVAR:
                return "im.forsyth.lazync.Inferno.terror(instance)";
            case BOOLEAN:
                return "boolean.class";
            case BYTE:
                return "byte.class";
            case SHORT:
                return "short.class";
            case INT:
                return "int.class";
            case LONG:
                return "long.class";
            case CHAR:
                return "char.class";
            case FLOAT:
                return "float.class";
            case DOUBLE:
                return "double.class";
            default:
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        "Invalid param type: ", targetElement);
                return null;
        }
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(Lazync.class.getName()));
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }
}
